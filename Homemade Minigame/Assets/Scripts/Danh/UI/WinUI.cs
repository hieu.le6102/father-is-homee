﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class WinUI : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject pannelRed;
    public GameObject pannelGreen;
    public HudController hud;
    void Start()
    {
        pannelRed.SetActive(false);
        pannelGreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void NotifiWin(string side)
    {

        hud.SetDefault();
        if (side == "Red")
        {
            print("Green Win");
            pannelGreen.SetActive(true);
        }
        if (side == "Blue")
        {
            print("Red Win");
            pannelRed.SetActive(true);
        }
    }
     
}
