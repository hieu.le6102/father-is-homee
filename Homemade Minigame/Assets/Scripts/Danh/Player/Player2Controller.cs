﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Controller : MonoBehaviour
{
    public Rigidbody2D body;
    public Transform tf;
    public HudController hud;

    public WinUI winUI;
    // Start is called before the first frame update
    void Start()
    {
        body.gravityScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.I))
        {
            print("Press I");
            body.AddForce(Vector2.up * hud.speedSlice * hud.speedSlice * hud.speedSlice);
        }
        else if (Input.GetKey(KeyCode.K))
        {
            print("Press K");
            body.AddForce(Vector2.down * hud.speedSlice * hud.speedSlice * hud.speedSlice);
        }
    }

    public void KeyCodeI()
    {
        body.AddForce(Vector2.up * hud.speedSlice * hud.speedSlice * hud.speedSlice);
    }

    public void KeyCodeK()
    {
        body.AddForce(Vector2.down * hud.speedSlice * hud.speedSlice * hud.speedSlice);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        winUI.NotifiWin("Red");
    }

}
