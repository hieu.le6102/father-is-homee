﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum MovementDirection {Up, Down, Left, Right, Dash}
public class PlayerMovement : MonoBehaviour
{
    public MovementDirection direction;
    public KeyCode[] viableInputs;

    // Start is called before the first frame update
    
    public Vector2 Value { get; private set; }

    internal virtual void Update()
    {
        foreach(var key in viableInputs)
        {
            if (Input.GetKey(key))
            {
                Value = GetDirection(direction);
                return;
            }
        }

        Value = Vector2.zero;
    }

    internal Vector2 GetDirection (MovementDirection direction)
    {
        switch (direction)
        {
            case MovementDirection.Up:
                print("Up");
                return new Vector2(0f, 1f);
            case MovementDirection.Down:
                print("Down");
                return new Vector2(0f, -1f);
            case MovementDirection.Left:
                print("Left");
                return new Vector2(-1f, 0f);
            case MovementDirection.Right:
                print("Right");
                return new Vector2(1f, 0f);

        }

        throw new System.Exception("Invalid movement direction");
    }

    // // Update is called once per frame
    // void Update()
    // {
        
    // }
}
