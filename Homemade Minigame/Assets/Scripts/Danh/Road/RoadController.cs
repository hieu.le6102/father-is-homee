﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadController : MonoBehaviour
{
    public Rigidbody2D body;
    Vector2 offset;
    public HudController hud;
    // Start is called before the first frame update
    void Start()
    {
        body.gravityScale = 0;
    }

    // Update is called once per frame
    void Update()
    {
        body.AddForce(Vector2.left * hud.speedRoad);
    }
}
