﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public Transform[] spawnPlayer1Point;
    public Transform[] spawnPlayer2Point;
    public GameObject[] enemyPrefabs;
    public SpriteRenderer sprite; 
    public List<GameObject> activeEnemy = new List<GameObject>();
    public Transform playerTransform;
    public int CountEnemy;
    void Start()
    {
        SpawnEnemy(0, 1);
        SpawnEnemy(0, 0);
    }
    void Update()
    {

    }
   
    public void SpawnEnemy(int enemyIndex, int isUp)
    {
        
        if (isUp == 1)
        {
            int index = Random.Range(0, spawnPlayer1Point.Length);
            GameObject enemy = (GameObject)Instantiate(enemyPrefabs[enemyIndex], spawnPlayer1Point[index].position, spawnPlayer1Point[index].rotation); 
            enemy.tag = "EnemyUp";
            activeEnemy.Add(enemy);
        }
        else
        {
            int index = Random.Range(0, spawnPlayer2Point.Length);
            GameObject enemy = (GameObject)Instantiate(enemyPrefabs[enemyIndex], spawnPlayer2Point[index].position, spawnPlayer2Point[index].rotation); 
            enemy.tag = "EnemyDown";
            activeEnemy.Add(enemy);
        }
    }

    public void DeleteEnemy()
    {
        Destroy(activeEnemy[0]);
        activeEnemy.RemoveAt(0);
        CountEnemy += 1;
    }
}
