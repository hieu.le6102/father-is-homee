﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HudController : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed;
    public float speedRoad;
    public float speedEnemy;
    public float speedPlayerInMainBoard;
    public float hardRate;
    public float speedSlice;
    public EnemyManager enemyManager;
    void Start()
    {
        SetStart();
    }

    // Update is called once per frame
    void Update()
    {
        speedEnemy = 0.4f + hardRate*enemyManager.CountEnemy;
    }

    public void SetDefault()
    {
        speed = 0f;
        speedEnemy = 0f;
        speedRoad = 0f;
        speedSlice = 0f;
        hardRate = 0f;

    }

    public void SetStart()
    {
        speed = 0.3f;
        speedEnemy = 0.6f;
        speedRoad = 0.3f;
        speedSlice = 3.0f;
        hardRate = 0f;

    }

}
